using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Web : MonoBehaviour
{
    // Start is called before the first frame update
    CooldownManager death_cooldown; // death cooldown
    Animator animator;

    void Start()
    {
        animator = GetComponent<Animator>();
        death_cooldown = GetComponent<CooldownManager>();
        death_cooldown.attack_cooldown = 5;
        death_cooldown.reset_Cooldown();
    }

    void Update()
    {
        // trap flies done in fly

        // kill it
        if (death_cooldown.off_Cooldown()) {
            StartCoroutine(KillWeb());
        }
    }

    public IEnumerator KillWeb()
    {
        animator.SetTrigger("kill_web");
        yield return new WaitForSeconds(0.55f);
        Destroy(this.gameObject);
        yield return null;
    }
}
