using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebberCombat : MonoBehaviour
{
    public GameObject web_prefab;
    public GameObject web_launch_point;

    public CooldownManager cooldown;
    public Animator animator;
    
    // Start is called before the first frame update
    void Start()
    {   
        cooldown = GetComponent<CooldownManager>();
        cooldown.attack_cooldown = 10;  
        animator = GetComponent<Animator>();   
    }

    // Update is called once per frame
    void Update()
    {
        if (cooldown.off_Cooldown()) {
            StartCoroutine(Spawn_Web());
            cooldown.reset_Cooldown();
        }
    }

    public IEnumerator Spawn_Web() {
        // make attack animation
        // wait a tiny bit
        animator.SetTrigger("has_target");
        yield return new WaitForSeconds(0.32f);
        
        Vector3 web_pos = web_launch_point.transform.position;
        web_pos.z = -0.5f;
        // instantiate the web
        Instantiate(web_prefab, web_pos, this.transform.rotation);
        yield return null;
    }
}
