using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    // spawn types - have to have these and static ones
    public GameObject _grabber;
    public GameObject _grabber_ghost;
    
    public GameObject _shooter;
    public GameObject _shooter_ghost;

    public GameObject _webber;
    public GameObject _webber_ghost;

    public GameObject _sprayer;
    public GameObject _sprayer_ghost;

    public static GameObject Grabber;
    public static GameObject Grabber_Ghost;    
    public static GameObject Shooter;   
    public static GameObject Shooter_Ghost;

    public static GameObject Sprayer;
    public static GameObject Sprayer_Ghost;

    public static GameObject Webber;
    public static GameObject Webber_Ghost;
    
    void Awake()
    {
        Grabber = _grabber;
        Grabber_Ghost = _grabber_ghost;
        Shooter = _shooter;
        Shooter_Ghost = _shooter_ghost;
        Webber = _webber;
        Webber_Ghost = _webber_ghost;
        Sprayer = _sprayer;
        Sprayer_Ghost = _sprayer_ghost;
    }
    public static GameObject get_Ghost(string type)
    {
        if (type.ToLower() == "grabber") return Grabber_Ghost;
        if (type.ToLower() == "shooter") return Shooter_Ghost;
        if (type.ToLower() == "webber") return Webber_Ghost;
        if (type.ToLower() == "sprayer") return Sprayer_Ghost;
        
        return null;
    }

    public static GameObject get_Spider(string type)
    {
        if (type.ToLower() == "grabber") return Grabber;
        if (type.ToLower() == "shooter") return Shooter;
        if (type.ToLower() == "webber") return Webber;
        if (type.ToLower() == "sprayer") return Sprayer;

        return null;
    }

    public static void Spawn_Spider(Vector3 location, string type)
    {
        GameObject spider = get_Spider(type);
        location.z = -1;
        Instantiate(spider, location, Grabber.transform.rotation);

    }
    
}
