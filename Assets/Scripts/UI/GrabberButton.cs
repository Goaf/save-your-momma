using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabberButton : MonoBehaviour
{   public void ButtonClick(string type) {
      
      Debug.Log("Clicked " + type + " button!");
      // check if we can purchase
      if (!Inventory.Can_Buy_Spider(type)) return;
      
      // get the correct ghost type
      GameObject ghost_type = SpawnManager.get_Ghost(type);

      // ghost position     
      Vector3 ghost_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
      
      // instantiate ghost
      GameObject new_ghost = Instantiate(ghost_type, ghost_pos, ghost_type.transform.rotation);
      new_ghost.GetComponent<Ghost>().ghost_type = type;
      
      // disable all butyons
      Inventory.Disable_Buttons();
   }

   
}
