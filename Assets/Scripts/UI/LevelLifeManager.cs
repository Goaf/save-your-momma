using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLifeManager : MonoBehaviour
{
    public Queen queen;
    public FlyMaker fly_maker;

    void Start()
    {
        queen = FindObjectOfType<Queen>();
        fly_maker = FindObjectOfType<FlyMaker>();
    }

    // Update is called once per frame
    void Update()
    {
        if (queen.health <= 1) SwitchScene.RestartScene();
        else if (fly_maker.all_spawned()) {
            // if all flys have been spawned, make sure that
            // none are left (ie all have been killed)
            if (FindObjectsOfType<FlyCombat>().Length == 0) SwitchScene.NextScene();
        } 
    }
}
