using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour
{
    public string ghost_type;
    public GameObject backing_square_red;
    public GameObject backing_square_green;

    public bool on_spider = false;
    public bool allowed_to_place = false;

    // Update is called once per frame
    void Update()
    {
        addTint();

        // do nothing if user presses esc
        if (Input.GetKeyDown(KeyCode.Escape)) {
            Inventory.Enable_Buttons();
            Destroy(this.gameObject);            
        }

        // Follow the mouse
        Vector3 mouse_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mouse_pos.z = -1;
        transform.position = mouse_pos;

        // actually put spider down
        if (Input.GetMouseButtonDown(0) && allowed_to_place && (!on_spider)) {
            SpawnManager.Spawn_Spider(mouse_pos, ghost_type);
            Inventory.Buy_Spider(ghost_type);
            Inventory.Enable_Buttons();
            Destroy(this.gameObject);
        }



    }

    void addTint() {
        
        backing_square_red.GetComponent<SpriteRenderer>().enabled = !(allowed_to_place && (!on_spider));
        backing_square_green.GetComponent<SpriteRenderer>().enabled = allowed_to_place && (!on_spider);
        
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.tag.ToLower() == ghost_type + "zone") {
            allowed_to_place = true;
        }
    }

    void OnTriggerStay2D(Collider2D other) {
        if (other.gameObject.tag.ToLower() == "spider") {
            on_spider = true;
        }
    }

    void OnTriggerExit2D(Collider2D other) {
        if (other.gameObject.tag.ToLower() == ghost_type + "zone") {
            allowed_to_place = false;
        }
        if (other.gameObject.tag.ToLower() == "spider") {
            on_spider = false;
        } 
    }

    
}
