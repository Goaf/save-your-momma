using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndCutscene : MonoBehaviour
{
    // Start is called before the first frame update
    public int scene_num;
    public float swap_time = 30.0f;

    void Awake()
    {
        StartCoroutine(SkipScene());
    }

    public IEnumerator SkipScene()
    {
        yield return new WaitForSeconds(swap_time);
        SwitchScene.Switch(scene_num);
        yield return null;
    }
}
