using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopAudio : MonoBehaviour
{
    
    // Start is called before the first frame update
    
    public AudioSource audioSource;
    void Start()
    {
        audioSource.Play();
        audioSource.loop = true;
        
    }

   
}
