using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SwitchScene : MonoBehaviour
{
    // Start is called before the first frame update
    static public void Switch(int scene_num)
    {
        SceneManager.LoadScene(scene_num);   
    }

    static public void NextScene()
    {
        Switch(SceneManager.GetActiveScene().buildIndex + 1);
    }

    static public void RestartScene()
    {
        Switch(SceneManager.GetActiveScene().buildIndex);
    }
}
