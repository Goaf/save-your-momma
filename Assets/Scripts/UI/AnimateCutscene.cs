using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateCutscene : MonoBehaviour
{
    public Animator animator;
    public float swap_time = 15.0f;
    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Awake()
    {
        StartCoroutine(SkipToNext());
    }

    public IEnumerator SkipToNext()
    {
        yield return new WaitForSeconds(swap_time);
        animator.SetTrigger("skip_to_next");
        yield return null;
    }
}
