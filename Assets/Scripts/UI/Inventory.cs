using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Inventory : MonoBehaviour
{
    public int start_num_flies = 6;
    
    public static int num_flies = 100;
    public static int SHOOTER_COST = 3;
    public static int GRABBER_COST = 2; 
    public static int WEBBER_COST = 8;    
    public static int SPRAYER_COST = 10;
    // Static Helpers
    
    void Start()
    {
        num_flies = start_num_flies; 
    }
    
    public static void Disable_Buttons() 
    {
        Button [] buttons = FindObjectsOfType<Button>();
        foreach (Button button in buttons) {
            button.enabled = false;
        }
    }
    
    public static void Enable_Buttons()
    {
        Button [] buttons = FindObjectsOfType<Button>();
        foreach (Button button in buttons) {
            button.enabled = true;
        }
    }

    public static int get_Cost(string type)
    {
        if (type.ToLower() == "grabber") {
            return GRABBER_COST;          
        }
        
        if (type.ToLower() == "shooter") {
            return SHOOTER_COST;
        }

        if (type.ToLower() == "webber") {
            return WEBBER_COST;
        }

        if (type.ToLower() == "sprayer") {
            return SPRAYER_COST;
        }

        else return 100;
    }

    public static bool Can_Buy_Spider(string type)
    {        
        return (num_flies >= get_Cost(type));
            
    }
    
    public static bool Buy_Spider(string type)
    {        
        if (num_flies >= get_Cost(type)) {
            num_flies -= get_Cost(type);
            return true;
        } 
        
        return false;
    }

    public static int get_Num_Flies()
    {
        return num_flies;
    }

    public static void increment_Num_Flies(int increment)
    {
        num_flies += increment;
    }

    

}
