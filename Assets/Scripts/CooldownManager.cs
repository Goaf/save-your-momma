using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CooldownManager : MonoBehaviour
{
    public float attack_cooldown = 1.5f; 
    private bool can_attack = true;
    private float time_of_last_attack = 0.0f;

    public bool off_Cooldown() {
        if ((Time.time - time_of_last_attack) >= attack_cooldown) can_attack = true;
        return can_attack;
    }

    public void reset_Cooldown() {
        can_attack = false;
        time_of_last_attack = Time.time;
    }
}
