using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Queen : MonoBehaviour
{
    public float health;
    public float max_health = 20.0f;
    public float fly_damage = 1.0f;

    public HealthBar healthbar;
    // Start is called before the first frame update
    public Animator animator; 
    void Start()
    {
        health = max_health;
        animator = GetComponent<Animator>();    
    }

    void Update()
    {
        healthbar.SetHealth(health / max_health);
    }

    
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy") {
            col.gameObject.GetComponent<FlyCombat>().Kill();
            animator.SetTrigger("queen_damage");
            health -=1;
        }
    }
}
