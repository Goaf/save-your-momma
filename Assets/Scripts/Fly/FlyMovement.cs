using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FlyMovement : MonoBehaviour
{

    public Vector2 target = new Vector2(0, 0);
    public float base_speed = 1.3f;
    public float speed;
    private float slow_speed = 0.5f;

    Vector2 random_dir = new Vector2(0, 0);
    float random_dir_scale = 0.3f;
    
    CooldownManager jiggle_cooldown;
    CooldownManager poison_cooldown;

    private float poison_damage = 1.0f;

    void Start()
    {
        jiggle_cooldown = GetComponent<CooldownManager>(); // cooldown for random lerb
        jiggle_cooldown.attack_cooldown = 0.3f;

        poison_cooldown = gameObject.AddComponent(typeof(CooldownManager)) as CooldownManager;
        poison_cooldown.attack_cooldown = 1.0f;

        speed = base_speed;
    }

    Vector2 getRandomDirection()
    {
        if (jiggle_cooldown.off_Cooldown()) {
            random_dir = Random.insideUnitCircle;
            jiggle_cooldown.reset_Cooldown();
        } 

        return random_dir;       
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Web") {
            
            speed = slow_speed;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag == "Poison") {            
            if (poison_cooldown.off_Cooldown()) {
                poison_cooldown.reset_Cooldown();
                gameObject.GetComponent<FlyCombat>().Damage(poison_damage);
            }
        }
    }


    void OnTriggerExit2D(Collider2D other)
    {        
        if (other.gameObject.tag == "Web") {
            speed = base_speed;
        }
    }

    void Update()
    {
        // Move towards the target
        //Vector3 direction = (target - transform.position).normalized;
        Vector2 direction = (Vector2.right + getRandomDirection() *random_dir_scale).normalized;
        // TODO: add a bobble
        transform.Translate(direction * speed * Time.deltaTime); 

    }




}
