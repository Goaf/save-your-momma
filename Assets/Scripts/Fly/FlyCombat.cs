using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyCombat : MonoBehaviour
{   

    public AudioSource death_sound;
    public CooldownManager poison_cooldown;

    private FlyMaker fly_maker;
    Animator animator;
    void Start()
    {
        fly_maker = FindObjectOfType<FlyMaker>();
        animator = GetComponent<Animator>();
          
    }
    private float health = 5;

    public void Kill() {
        Damage(health);
    }

    public void Damage(float dmg) {
        this.health -= dmg;
        if (this.health <= 0) {
            StartCoroutine(KillFly());
        }
    }

    
    public IEnumerator KillFly() {
        // TODO: play death animation
        animator.SetTrigger("fly_death");
        death_sound.Play();
        yield return new WaitForSeconds(0.25f);
        Debug.Log("Killing him!");
        Inventory.increment_Num_Flies(1);
        fly_maker.num_killed++;
        Destroy(this.gameObject);
        yield return null;
    }

}
