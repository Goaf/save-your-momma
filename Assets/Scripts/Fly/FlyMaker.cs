using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyMaker : MonoBehaviour
{
    
    public GameObject fly;
    private CooldownManager cooldown;
    public Transform target;

    public int num_flys;
    private int num_spawned = 0;
    // Start is called before the first frame update

    public float spawn_velocity;
    public float spawn_1_velocity = 1.3f;
    public float spawn_2_velocity = 1.3f;
    public float spawn_3_velocity = 1.3f;

    public int spawn1_size = 3;
    public int spawn2_size = 5;
    public int spawn3_size = 8;
    public float between_spawn_wait_time = 10.0f;

    public bool spawn_initial = true;
    public float initial_wait_time = 20.0f;  
    public bool done_waiting = false;  
    
    public float spawn_cooldown = 3.0f;
    public int num_killed = 0;

    void Start()
    {
        spawn_velocity = spawn_1_velocity;
        num_flys = spawn1_size + spawn2_size + spawn3_size;
        cooldown = GetComponent<CooldownManager>();
        cooldown.attack_cooldown = spawn_cooldown;
        // spawn initial
        if (spawn_initial) SpawnInitialFly();
        // wait for rest
        StartCoroutine(Wait(initial_wait_time));
    }

    void SpawnInitialFly()
    {
        SpawnFly(0.6f);
        return;
    }

    void checkSpawns()
    {    
        int count = num_spawned;
        if (count  == spawn1_size) {
            spawn_velocity = spawn_2_velocity;
            StartCoroutine(Wait(between_spawn_wait_time));
        } else if (count == spawn1_size + spawn2_size) {
            spawn_velocity = spawn_2_velocity;
            StartCoroutine(Wait(between_spawn_wait_time));
        } else if (count == spawn1_size + spawn2_size + spawn3_size) {
            spawn_velocity = spawn_3_velocity;
            StartCoroutine(Wait(between_spawn_wait_time));
        }
    }

    // Update is called once per frame
    void Update()
    {
        
        if (!done_waiting) return;        

        if (cooldown.off_Cooldown() && num_spawned < num_flys) {
            num_spawned++;
            cooldown.reset_Cooldown();
            SpawnFly(spawn_velocity);            
        }

        checkSpawns();
        
    }

    public IEnumerator Wait(float wait_time)
    {
        done_waiting = false;
        yield return new WaitForSeconds(wait_time);
        done_waiting = true;
        yield return null;
    }

    void SpawnFly(float vel)
    {
        GameObject new_fly = Instantiate(fly,
                transform.position, fly.transform.rotation);
        new_fly.GetComponent<FlyMovement>().base_speed = vel;
        new_fly.GetComponent<FlyMovement>().speed = vel;
        
    }

    public bool all_spawned() {        
        return num_spawned >= num_flys;
        
    }
}
