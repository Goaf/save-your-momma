using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprayerAttack : MonoBehaviour
{
    public GameObject spray_prefab;
    public Transform spray_start;
    private Animator animator;
    
    private CooldownManager cooldown;

    void Awake()
    {
        animator = GetComponent<Animator>();
        cooldown = GetComponent<CooldownManager>();
        cooldown.attack_cooldown = 8.0f;
    
    }

    public void Attack() {
        if (cooldown.off_Cooldown()) StartCoroutine(Spray());
    }
    public IEnumerator Spray() {
        // Animation Handling

        animator.SetTrigger("has_target");
        yield return new WaitForSeconds(0.35f);
        
        // Instantiation        
        Instantiate(spray_prefab, spray_start.position,
            transform.rotation);

        cooldown.reset_Cooldown();
        
        yield return null;
    }
}
