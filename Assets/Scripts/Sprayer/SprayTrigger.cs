using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprayTrigger : MonoBehaviour
{
    public GameObject sprayer;
   
    void OnTriggerEnter2D(Collider2D col)
    {
        
        if (col.gameObject.tag == "Enemy") {
            sprayer.GetComponent<SprayerAttack>().Attack();
        }
        
    }
}
