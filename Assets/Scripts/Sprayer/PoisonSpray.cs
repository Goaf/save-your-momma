using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoisonSpray : MonoBehaviour
{
    // Start is called before the first frame update
    CooldownManager cooldown;
    Animator animator;
    void Start()
    {
        animator = GetComponent<Animator>();
        cooldown = GetComponent<CooldownManager>();        
        cooldown.attack_cooldown = 3.0f;
        cooldown.reset_Cooldown();
    }

    // Update is called once per frame
    void Update()
    {
        if (cooldown.off_Cooldown()) StartCoroutine(KillSpray());
    }

    public IEnumerator KillSpray() {
        // play death animation
        animator.SetTrigger("kill_spray");
        
        yield return new WaitForSeconds(0.5f);
        Destroy(this.gameObject);
        yield return null;
    }
}
