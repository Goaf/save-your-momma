using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterAttack : MonoBehaviour
{
    public GameObject ball_prefab;
    public Transform shooter_ball_start;
    private Animator animator;
    
    private CooldownManager cooldown;

    public float attack_radius = 4.5f;  


    void Awake()
    {
        animator = GetComponent<Animator>();
        cooldown = GetComponent<CooldownManager>();
        cooldown.attack_cooldown = 3.0f;
    
    }

    // Update is called once per frame
    void Update()
    {
        if (cooldown.off_Cooldown()) {
            // Find enemies
            GameObject[] nearby_enemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject enemy in nearby_enemies) {
                if (Vector3.Distance(enemy.transform.position,
                    transform.position) <= attack_radius) {
                        cooldown.reset_Cooldown();
                        StartCoroutine(Attack(enemy.transform));
                        break;
                    }
            }
        }       
        
    }

    public IEnumerator Attack(Transform target) {
        // Animation Handling
        // Setting direction of target
        Vector3 aim_direction = (target.position - shooter_ball_start.position).normalized;
        
        animator.SetTrigger("has_target");
        yield return new WaitForSeconds(0.25f);

       
        
        // Instantiation        
        GameObject shooter_ball = Instantiate(ball_prefab, shooter_ball_start.position,
            shooter_ball_start.rotation);
        shooter_ball.GetComponent<ShooterBall>().direction = aim_direction;

        
        yield return null;
    }
}
