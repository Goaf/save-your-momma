using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabberAttack : MonoBehaviour
{
    private CooldownManager cooldown;
    private Animator animator;

    private float damage = 2.5f;

    void Awake()
    {
        // TODO: set cooldowns
        animator = GetComponent<Animator>();
        cooldown = GetComponent<CooldownManager>();
        cooldown.attack_cooldown = 2.75f;
    }   
    
    public void fly_Trigger(FlyCombat fly) {
        // called when the fly trigger is triggered.
        // if we're off cooldown, set the animation trigger and then
        // reset the cooldown.
        if (cooldown.off_Cooldown()){
            animator.SetTrigger("has_target");
            fly.Damage(damage);
            cooldown.reset_Cooldown();
        }
        
    }
    
}
