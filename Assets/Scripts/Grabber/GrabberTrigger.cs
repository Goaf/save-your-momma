using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GrabberTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    public GrabberAttack grabber;

    void Awake()
    {
        grabber = transform.parent.gameObject.GetComponent<GrabberAttack>();
    }

    void OnTriggerStay2D(Collider2D col)
    {
        // called on trigger stay in case the fly remains inside the trigger range
        if (col.gameObject.tag == "Enemy") {
            grabber.fly_Trigger(col.gameObject.GetComponent<FlyCombat>());
        }
        
    }
}
