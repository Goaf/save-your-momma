using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterBall : MonoBehaviour
{

    public Vector2 direction = new Vector2(0, 0); // normalised direction vector to the target
    private float speed = 20.0f; // speed at which the projectile travels 

    public float damage = 2.0f;
    public CooldownManager out_of_range; // kills them if they go out of range
    public Animator animator;
    void Start()
    {
        out_of_range = gameObject.AddComponent(typeof(CooldownManager)) as CooldownManager;
        out_of_range.attack_cooldown = 5.0f; // destroy 5s off screen
        out_of_range.reset_Cooldown();
        animator = GetComponent<Animator>();
    }    
    // Update is called once per frame    
    void Update()
    {
        if (out_of_range.off_Cooldown()) Destroy(this.gameObject);
        transform.Translate(direction * speed * Time.deltaTime);        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Enemy") {
            col.gameObject.GetComponent<FlyCombat>().Damage(damage);
            StartCoroutine(ExplodeBall());
        }

    }

     public IEnumerator ExplodeBall() {
        // TODO: play death animation
        animator.SetTrigger("kill_ball");
        speed = 0.0f;
        yield return new WaitForSeconds(0.25f);
        Destroy(this.gameObject);
        yield return null;
    }


    
}
